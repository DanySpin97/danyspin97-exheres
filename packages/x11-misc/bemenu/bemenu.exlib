# Copyright 2019-2020 Danilo Spinella <danyspin97@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user='Cloudef' ]
export_exlib_phases src_prepare src_compile src_install

SUMMARY="Dynamic menu library and client inspired by dmenu"

LICENCES="GPL-3 LGPL-3"
SLOT="0"

MYOPTIONS="
    (
        ncurses
        X
        wayland
    ) [[ number-selected = at-least-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        ncurses? (
            sys-libs/ncurses
        )
        X? (
            x11-libs/cairo
            x11-libs/pango
            x11-libs/libX11
            x11-libs/libXinerama
        )
        wayland? (
            sys-libs/wayland
            x11-libs/cairo
            x11-libs/pango
            x11-libs/libxkbcommon[wayland]
        )
"

DEFAULT_SRC_COMPILE_PARAMS=(
    PREFIX=/usr/$(exhost --target)
    DESTDIR="${IMAGE}"
)

bemenu_src_prepare() {
    default

    edo sed -e "s/pkg-config/$(exhost --tool-prefix)pkg-config/" \
            -i "${WORK}"/GNUmakefile
}

bemenu_src_compile() {
    local params=
    if optionq ncurses; then
        params+="curses"
    fi
    if optionq X; then
        params+=" x11"
    fi
    if optionq wayland; then
        params+=" wayland"
    fi

    emake "${DEFAULT_SRC_COMPILE_PARAMS[@]}" ${params[@]} clients
}

bemenu_src_install() {
    local params=
    if optionq ncurses; then
        params+="install-curses"
    fi
    if optionq X; then
        params+=" install-x11"
    fi
    if optionq wayland; then
        params+=" install-wayland"
    fi

    emake "${DEFAULT_SRC_COMPILE_PARAMS[@]}" ${params[@]} \
          install-include install-pkgconfig install-bins \
          install-libs install-lib-symlinks

    emake DESTDIR="${IMAGE}" PREFIX=/usr install-man
}

