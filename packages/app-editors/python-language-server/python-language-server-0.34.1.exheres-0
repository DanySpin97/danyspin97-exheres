# Copyright 2018 Danilo Spinella <danyspin97@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ import='setuptools' blacklist="2" test=pytest ]

SUMMARY="An implementation of the Language Server Protocol for Python."

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/pluggy[python_abis:*(-)?]
        dev-python/python-jsonrpc-server[>=0.3.2][python_abis:*(-)?]
        dev-python/ujson[<=1.35][python_abis:*(-)?]
        dev-util/jedi[>=0.17.0&<0.18.0][python_abis:*(-)?]
    recommendation:
        dev-python/flake8[>=3.8.0][python_abis:*(-)?]
        dev-python/mccabe[>=0.6.0&<0.7.0][python_abis:*(-)?]
        dev-python/pycodestyle[>=2.6.0&<2.7.0][python_abis:*(-)?]
        dev-python/pydocstyle[>=2.0.0][python_abis:*(-)?]
        dev-python/pyflakes[>=2.2.0&<2.3.0][python_abis:*(-)?]
        dev-python/pylint[python_abis:*(-)?]
        dev-python/rope[>=0.10.5][python_abis:*(-)?]
        dev-python/yapf[python_abis:*(-)?]
    suggestion:
        dev-python/autopep8[python_abis:*(-)?]
    test:
        dev-python/autopep8[python_abis:*(-)?]
        dev-python/coverage[python_abis:*(-)?]
        dev-python/mock[python_abis:*(-)?]
        dev-python/pydocstyle[python_abis:*(-)?]
        dev-python/pylint[python_abis:*(-)?]
        dev-python/pytest-cov[python_abis:*(-)?]
        dev-python/tox[python_abis:*(-)?]
"

test_one_multibuild() {
    local disabled_tests=(
        # Avoid quite heavy test dependencies
        "not test_numpy_completions"
        "and not test_matplotlib_completions"
        "and not test_pandas_completions"
        "and not test_pyqt_completion"

        # These tests require numpy as well
        "and not test_snippet_parsing"
        "and not test_numpy_hover"
    )
    export PYTEST_PARAMS=( -k "${disabled_tests[*]}" )

    # Some tests only work in a special environment used in the CI of the project. These tests are
    # skipped based on the presence of the CI variable. Unfortunately other environments like GitLab
    # use this variable too. Make sure it's not set to avoid these tests
    unset CI

    setup-py_test_one_multibuild
}

